from django.contrib.auth.models import User, Group
from rest_framework import serializers

from .models import Classroom, Request, SO, PC, SW, PC_SO_SW, Reservation, SW_Classroom
from django.contrib.auth.models import User


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class ClassroomSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.id')

    class Meta:
        model = Classroom
        fields = ('id', 'url', 'owner', 'number', 'floor', 'address', 'feature', 'capacity')


class SOSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SO
        fields = ('id', 'name', 'description')


class PCSerializer(serializers.ModelSerializer):

    class Meta:
        model = PC
        fields = ('id', 'number', 'details', 'so')
        depth = 1


class PC_SO_SWSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = PC_SO_SW
        fields = ('id', 'pc', 'so', 'sw')

class SW_ClassroomSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = SW_Classroom
        fields = ('id', 'number', 'sw')

class RequestSerializer(serializers.ModelSerializer):
    # classroom = serializers.HyperlinkedIdentityField(view_name='classroom-detail', )

    class Meta:
        model = Request
        fields = ('id', 'requirements', 'status', 'slots_set')


class ReservationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Reservation
        fields = ('name', 'request')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    # classrooms = serializers.HyperlinkedRelatedField(many=True, view_name='classroom-detail', read_only=True)

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'classrooms')


class SWSerializer(serializers.HyperlinkedModelSerializer):
    """docstring for SWSerilizer"""
    class Meta:
        model = SW
        fields = ('id', 'name', 'description')
        


