from django.db import models
from django.urls import reverse
from django.contrib.auth.models import AbstractUser
import datetime
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError


# implementacion basada en: https://simpleisbetterthancomplex.com/tutorial/2018/01/18/how-to-implement-multiple-user-types-with-django.html


class User(AbstractUser):
    # bandera que indica el tipo de usuario
    is_admin = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)
    is_becario = models.BooleanField(default=False)

    # USER_TYPE_CHOICES = (
    #   (1, 'student'),
    #   (2, 'teacher'),
    #   (3, 'secretary'),
    #   (4, 'supervisor'),
    #   (5, 'admin'),
    # )

    # user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES)


class Establishment(models.Model):
    name = models.CharField(default='', max_length=100)
    address = models.CharField(default='', max_length=100)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('establecimientoDetail', kwargs={"pk": self.id})


class Classroom(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    number = models.CharField(max_length=5, unique=True)
    floor = models.PositiveIntegerField(default=0)
    address = models.ForeignKey(Establishment, on_delete=models.CASCADE)

    capacity = models.PositiveIntegerField(default=0)
    feature = models.CharField(default='', max_length=100, null=True, blank=True)

    # owner = models.ForeignKey('auth.User', default='', related_name='classrooms', on_delete=models.CASCADE)

    class Meta:
        ordering = ('created',)

    def __str__(self):
        return (f'{self.number}')

    def get_absolute_url(self):
        return reverse('aulaDetail', kwargs={"pk": self.id})


class SO(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now=True)
    virtualized = models.BooleanField(
        verbose_name="Virtualizado", default=False)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('soDetail', kwargs={"pk": self.id})


class PC(models.Model):
    number = models.PositiveIntegerField()
    details = models.CharField(max_length=300)
    classroom = models.ForeignKey(Classroom, on_delete=models.CASCADE)
    so = models.ManyToManyField(SO, through='PC_SO', )
    created_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ['number', 'classroom']
    
    def __str__(self):
        return (f'{self.number} [Aula: {self.classroom}]')

    def get_absolute_url(self):
        return reverse('pcDetail', kwargs={"pk": self.id})


class PC_SO(models.Model):
    pc = models.ForeignKey(PC, on_delete=models.CASCADE)
    so = models.ForeignKey(SO, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['pc', 'so']

    def __str__(self):
        return str(self.pc) + " " + str(self.so)


class SW(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=300)
    pc_so = models.ManyToManyField(PC_SO, through='PC_SO_SW',)

    def get_absolute_url(self):
        return reverse('swDetail', kwargs={"pk": self.id})

    def __str__(self):
        return self.name

# No vamos a utilizar este modelo, vamos a unir pc a aula y pc a sw.
class SW_Classroom(models.Model):
    sw = models.ForeignKey(SW, verbose_name=u'SW', on_delete=models.CASCADE)
    classroom = models.ForeignKey(
        Classroom, verbose_name=u'Classroom', on_delete=models.CASCADE)

    def __str__(self):
        return self.sw


class PC_SO_SW(models.Model):
    pc_so = models.ForeignKey(
        PC_SO, verbose_name=u'PC', on_delete=models.CASCADE)
    sw = models.ForeignKey(SW, verbose_name=u'SW', on_delete=models.CASCADE)

    class Meta:
        unique_together = ['pc_so', 'sw']

    def __str__(self):
        return str(self.pc_so) + " " + str(self.sw)


class Request(models.Model):
    # se reemplaza "owner = models.ForeignKey('auth.User', on_delete=models.CASCADE)" por:
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    requirements = models.CharField(
        default='aire acondicionado', max_length=100)
    # new: recien creada, aun puede ser editarse
    # pending: pendiente de aprobación
    # rejected: rechazada
    # approved: aprobada
    STATUS_CHOICES = (('new', 'nueva'), ('deletedReservation', 'reserva elmininada'),
                      ('approved', 'aprobada'), ('rejected', 'rechazada'))
    status = models.CharField(choices=STATUS_CHOICES,
                              default='new', max_length=100)
    CHOICES = (('punctual', 'Puntual'), ('periodic', 'Periódica'))
    type = models.CharField(
        choices=CHOICES, max_length=20, default='punctual')
    # Frecuency of the repetition
    CHOICES_REP = (('weekly', 'Semana'), ('monthly', 'Mes'))
    repetition = models.CharField(
        choices=CHOICES_REP, max_length=20, null=True)
    # Last day of a reservation
    until = models.DateField(verbose_name="Hasta el día", null=True)
    amountOfStudents = models.PositiveIntegerField(
        verbose_name=u'cantidad_de_pcs', default=20, null=False)
    swsRequired = models.ManyToManyField(
        SW, verbose_name=u"software(s)_soliciado(s)", through="Request_SW", blank=True)
    comments = models.TextField(blank=True, null=True)

    def __str__(self):
        # return 'req: ' + str(self.id)
        return 'id_solicitud: {} ; usuario_solicitante: {}'.format(self.id, self.owner)


class Request_SW(models.Model):
    request = models.ForeignKey(
        Request, verbose_name=u"solicitud", on_delete=models.CASCADE)
    sw = models.ForeignKey(
        SW, verbose_name=u"software solicitado", on_delete=models.CASCADE)

    def __str__(self):
        return 'request: ' + str(self.request) + " sw: " + str(self.sw)


class Reservation(models.Model):
    name = models.CharField(max_length=80)
    classroom = models.ForeignKey(
        Classroom, verbose_name=u"Aula", on_delete=models.CASCADE)
    request = models.OneToOneField(Request, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


HOY = datetime.datetime.now().strftime("%d/%m/%Y")


class Slots(models.Model):
    request = models.ForeignKey(Request, on_delete=models.CASCADE)
    reservation = models.ForeignKey(
        Reservation, on_delete=models.SET_NULL, blank=True, null=True)
    day = models.DateField(default=HOY, verbose_name="Día")
    start_time = models.TimeField(
        default="13:00", verbose_name="Hora de inicio")
    end_time = models.TimeField(default="14:00", verbose_name="Hora de fin")

    class Meta:
        ordering = ('day',)

    def __str__(self):
        return (f'Request: {self.request} Day: {self.day} Start: {seld.start_time} End: {self.end_time}')

    def clean(self):
        if self.start_time > self.end_time:
            raise ValidationError(_('La hora de inicio debe ser previa a la hora de fin'))
