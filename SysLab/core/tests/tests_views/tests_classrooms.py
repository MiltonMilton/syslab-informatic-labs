from django.urls import reverse
from SysLab.core.views import *
from SysLab.core.models import *
from django.test import TestCase, Client, RequestFactory
from django.contrib.auth.models import AnonymousUser
from mixer.backend.django import mixer
import pytest

# manage.py test SysLab.core


@pytest.mark.django_db 
class TestViews(TestCase): 

    def setUp(self):
        self.client = Client()
        self.classrooms_url = reverse('aulas')

        self.admin = User.objects.create(username="admin",password="admin",is_admin=True)
        self.profesor = User.objects.create(username="profe",password="admin",is_teacher=True)
        self.becario = User.objects.create(username="becario",password="admin",is_becario=True)
        self.user = User.objects.create(username="user",password="admin")


    # debe estar logeado y ser admin, puede ingresar
    def test_classrooms_authenticated_admin_should_pass(self):
        request = RequestFactory().get(self.classrooms_url)
        request.user = self.admin

        response = classrooms(request)

        self.assertEquals(response.status_code, 200)

    # becario no debe poder ingresar
    def test_classrooms_authenticated_becario_should_not_pass(self):
        request = RequestFactory().get(self.classrooms_url)
        request.user = self.becario

        response = classrooms(request)

        self.assertEquals(response.status_code, 302)

    # profesor no debe poder ingresar
    def test_classrooms_authenticated_profesor_should_not_pass(self):
        request = RequestFactory().get(self.classrooms_url)
        request.user = self.profesor

        response = classrooms(request)

        self.assertEquals(response.status_code, 302)

