from django.test import SimpleTestCase
from django.urls import reverse, resolve
from SysLab.core.views import reservations, reservations_listado, reservation_edit, reservation_detail, reservation_delete
# para correr tests:
# python manage.py test SysLab.core

# referencia:
# https://www.youtube.com/watch?v=0MrgsYswT1c

# URLS to test:
#   url(r'^reservas/$', views.reservations, name='reservations'),
#   url(r'^reservas/listado/$', views.reservations_listado, name='reservations_listado'),
#   url(r'^reservas/(?P<pk>[0-9]+)/editar/$', views.reservation_edit, name='reservationEdit'),
#   url(r'^reservas/(?P<pk>[0-9]+)/$', views.reservation_detail, name='reservationDetail'),
#   url(r'^reservas/(?P<pk>[0-9]+)/borrar/$', views.reservation_delete, name='reservationDelete'),



class TestUrls(SimpleTestCase):

    def test_reservations_url_resolves(self):
        url = reverse('reservations')
        self.assertEquals(resolve(url).func, reservations)

    def test_reservations_listado_url_resolves(self):
        url = reverse('reservations_listado')
        self.assertEquals(resolve(url).func, reservations_listado)

    def test_reservationEdit_url_resolves(self):
        url = reverse('reservationEdit', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, reservation_edit)

    def test_reservationDetail_url_resolves(self):
        url = reverse('reservationDetail', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, reservation_detail)

    def test_reservationnDelete_url_resolves(self):
        url = reverse('reservationDelete', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, reservation_delete)

    

     