from django.test import SimpleTestCase
from django.urls import reverse, resolve
from SysLab.core.views import (
    slots_reservation_create,
    slots_request_create,
    slots_edit,
    slots_detail,
    APISlotDelete
)

# para correr tests:
# python manage.py test SysLab.core

# referencia:
# https://www.youtube.com/watch?v=0MrgsYswT1c

# URLS to test:
# Slots
#     url(r'^ranuras/crear/reserva/(?P<pk_reserva>[0-9]+)/$', views.slots_reservation_create, name='slotsReservationCreate'),
#     url(r'^ranuras/crear/request/(?P<pk_solicitud>[0-9]+)/$',views.slots_request_create, name='slotsRequestCreate'),
#     url(r'^ranuras/(?P<pk>[0-9]+)/editar/$', views.slots_edit, name='slotsEdit'),
#     url(r'^ranuras/(?P<pk>[0-9]+)/$', views.slots_detail, name='slotsDetail'),
#     url(r'^api/ranura/(?P<pk_slot>[0-9]+)/borrar/$', views.APISlotDelete.as_view(), name="apiSlotsDelete"),




class TestUrls(SimpleTestCase):

    def test_slotsReservationCreate_url_resolves(self):
        url = reverse('slotsReservationCreate', kwargs={'pk_reserva': 1})
        self.assertEquals(resolve(url).func, slots_reservation_create)

    def test_slotsRequestCreate_url_resolves(self):
        url = reverse('slotsRequestCreate', kwargs={'pk_solicitud': 1})
        self.assertEquals(resolve(url).func, slots_request_create)

    def test_slotsEdit_url_resolves(self):
        url = reverse('slotsEdit', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, slots_edit)

    def test_slotsDetail_url_resolves(self):
        url = reverse('slotsDetail', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, slots_detail)

    def test_apiSlotsDelete_url_resolves(self):
        url = reverse('apiSlotsDelete',kwargs={'pk_slot': 1})
        self.assertEquals(resolve(url).func.view_class, APISlotDelete)

