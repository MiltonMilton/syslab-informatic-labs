from django.test import SimpleTestCase
from django.urls import reverse, resolve
from SysLab.core.views import establishment_detail, establishment_delete, establishment_edit, establishment_create, establishments

# para correr tests:
# python manage.py test SysLab.core

# referencia:
# https://www.youtube.com/watch?v=0MrgsYswT1c

# URLS to test:
# establecimientos
#     url(r'^establecimientos/(?P<pk>[0-9]+)/$', views.establishment_detail, name='establecimientoDetail'),
#     url(r'^establecimientos/(?P<pk>[0-9]+)/borrar/$', views.establishment_delete, name='establecimientoDelete'),
#     url(r'^establecimientos/(?P<pk>[0-9]+)/editar/$', views.establishment_edit, name='establecimientoEdit'),
#     url(r'^establecimientos/crear/', views.establishment_create, name='establecimientoCreate'),
#     url(r'^establecimientos/', views.establishments, name='establecimientos'),



class TestUrls(SimpleTestCase):

    def test_establecimientoDetail_url_resolves(self):
        url = reverse('establecimientoDetail', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, establishment_detail)

    def test_establecimientoDelete_url_resolves(self):
        url = reverse('establecimientoDelete', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, establishment_delete)

    def test_establecimientoEdit_url_resolves(self):
        url = reverse('establecimientoEdit', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, establishment_edit)

    def test_establecimientoCreate_url_resolves(self):
        url = reverse('establecimientoCreate')
        self.assertEquals(resolve(url).func, establishment_create)

    def test_establecimientos_url_resolves(self):
        url = reverse('establecimientos')
        self.assertEquals(resolve(url).func, establishments)

     