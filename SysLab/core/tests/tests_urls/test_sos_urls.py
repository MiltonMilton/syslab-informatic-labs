from django.test import SimpleTestCase
from django.urls import reverse, resolve
from SysLab.core.views import so_detail, so_delete, so_edit, so_create, sos, so_by_pc

# para correr tests:
# python manage.py test SysLab.core

# referencia:
# https://www.youtube.com/watch?v=0MrgsYswT1c

# URLS to test:
# sos
#     url(r'^sos/(?P<pk>[0-9]+)/$', views.so_detail, name='soDetail'),
#     url(r'^sos/(?P<pk>[0-9]+)/borrar/$', views.so_delete, name='soDelete'),
#     url(r'^sos/(?P<pk>[0-9]+)/editar/$', views.so_edit, name='soEdit'),
#     url(r'^sos/crear/', views.so_create, name='soCreate'),
#     url(r'^sos/', views.sos, name='sos'),
#     url(r'^ajax/so/', views.so_by_pc, name='ajax_so_by_pc'),



class TestUrls(SimpleTestCase):

    def test_soDetail_url_resolves(self):
        url = reverse('soDetail', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, so_detail)

    def test_soDelete_url_resolves(self):
        url = reverse('soDelete', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, so_delete)

    def test_soEdit_url_resolves(self):
        url = reverse('soEdit', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, so_edit)

    def test_soCreate_url_resolves(self):
        url = reverse('soCreate')
        self.assertEquals(resolve(url).func, so_create)

    def test_sos_url_resolves(self):
        url = reverse('sos')
        self.assertEquals(resolve(url).func, sos)

    def test_ajax_so_by_pc_url_resolves(self):
        url = reverse('ajax_so_by_pc')
        self.assertEquals(resolve(url).func, so_by_pc)

     