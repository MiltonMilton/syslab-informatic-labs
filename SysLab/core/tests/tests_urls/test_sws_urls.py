from django.test import SimpleTestCase
from django.urls import reverse, resolve
from SysLab.core.views import (
    sw_detail, 
    sw_delete, 
    sw_edit, 
    sw_create, 
    sw_assign, 
    sw_assignment_delete, 
    sw_assignments,
    sw_so_classroom_assignment,
    sws
)

# para correr tests:
# python manage.py test SysLab.core

# referencia:
# https://www.youtube.com/watch?v=0MrgsYswT1c

# URLS to test:
# SW
#     url(r'^sws/(?P<pk>[0-9]+)/$', views.sw_detail, name='swDetail'),
#     url(r'^sws/(?P<pk>[0-9]+)/borrar/$', views.sw_delete, name='swDelete'),
#     url(r'^sws/(?P<pk>[0-9]+)/editar/$', views.sw_edit, name='swEdit'),
#     url(r'^sws/crear/', views.sw_create, name='swCreate'),
#     url(r'^sws/asignar/pc/', views.sw_assign, name='swAssign'),
#     url(r'^sws/asignaciones/(?P<pk>[0-9]+)/borrar/$', views.sw_assignment_delete, name='swAssignmentDelete'),
#     url(r'^sws/asignaciones/', views.sw_assignments, name='swAssignments'),
#     url(r'^sws/asignar/aula/', views.sw_so_classroom_assignment, name='swSOClassroomAssignment'),
#     url(r'^sws/', views.sws, name='sws'),



class TestUrls(SimpleTestCase):

    def test_swDetail_url_resolves(self):
        url = reverse('swDetail', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, sw_detail)

    def test_swDelete_url_resolves(self):
        url = reverse('swDelete', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, sw_delete)

    def test_swEdit_url_resolves(self):
        url = reverse('swEdit', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func, sw_edit)

    def test_swCreate_url_resolves(self):
        url = reverse('swCreate')
        self.assertEquals(resolve(url).func, sw_create)

    def test_swAssign_url_resolves(self):
        url = reverse('swAssign')
        self.assertEquals(resolve(url).func, sw_assign)

    def test_swAssignmentDelete_url_resolves(self):
        url = reverse('swAssignmentDelete', kwargs={'pk':1})
        self.assertEquals(resolve(url).func, sw_assignment_delete)

    def test_swAssignments_url_resolves(self):
        url = reverse('swAssignments')
        self.assertEquals(resolve(url).func, sw_assignments)

    def test_swSOClassroomAssignment_url_resolves(self):
        url = reverse('swSOClassroomAssignment')
        self.assertEquals(resolve(url).func, sw_so_classroom_assignment)

    def test_sws_url_resolves(self):
        url = reverse('sws')
        self.assertEquals(resolve(url).func, sws)
     