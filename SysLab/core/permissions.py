from rest_framework import permissions

class IsAdmin(permissions.BasePermission):
    """
    Allows access only to "is_admin" users.
    """
    def has_permission(self, request, view):
        return request.user.is_admin

class IsAdminOrBecario(permissions.BasePermission):
    """
    Allows access only to "is_admin" or "is_becario" users.
    """
    def has_permission(self, request, view):
        return request.user.is_admin or request.user.is_becario
