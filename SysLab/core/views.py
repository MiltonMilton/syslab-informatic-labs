from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework.generics import get_object_or_404
from django.shortcuts import redirect
from django.http import HttpResponseRedirect, Http404
from rest_framework.response import Response
from .decorators import admin_required, teacher_required, admin_or_teacher_required
from django.core.mail import send_mail, EmailMultiAlternatives
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from .forms import PcForm, SwForm, SoForm, SlotsForm, ClassroomForm, SwToPcForm, \
    EstablishmentForm, RequestForm, requestFormSet, SlotRequest, \
    RequestFormOne, RequestPeriodic, SlotsFormset, RequestToReservationForm, RequestSwFormset, SwToClassroomForm, \
    PcSoSwForm
from .models import Classroom, Request, SO, PC, SW, PC_SO, PC_SO_SW, Slots, Reservation, Establishment, Request_SW, SW_Classroom
from .serializers import ClassroomSerializer, RequestSerializer, UserSerializer, SOSerializer, PCSerializer, SW_ClassroomSerializer, SWSerializer, GroupSerializer, PC_SO_SWSerializer, ReservationSerializer
from django.forms import modelformset_factory
#permissions
from .permissions import IsAdmin, IsAdminOrBecario
# from rest_framework.response import Response
# from rest_framework.reverse import reverse
# from rest_framework.decorators import api_view
# from rest_framework import renderers
# from rest_framework import generics

# se reemplazafrom "django.contrib.auth.models import User" por
from django.contrib.auth import get_user_model
User = get_user_model()
from rest_framework import permissions
#Autenticacion
from django.contrib.auth.decorators import login_required
from django.urls import reverse
import datetime, copy

#User types
from django.contrib.auth import login
from django.shortcuts import redirect
from django.views.generic import CreateView
from .forms import LabUserSignUpForm

#api rango fechas
from datetime import date, timedelta, time
#para seleccion random de colores
import secrets

#editar reserva
import copy

import json

from rest_framework.views import APIView

HOY = datetime.datetime.now().strftime("%d/%m/%Y")

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (IsAdmin,)
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    permission_classes = (IsAdmin,)
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class ClassroomViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """

    permission_classes = (IsAdmin,)
    queryset = Classroom.objects.all()
    serializer_class = ClassroomSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class RequestViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdmin,)
    queryset = Request.objects.all()
    serializer_class = RequestSerializer


class SOViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    permission_classes = (IsAdmin,)
    queryset = SO.objects.all()
    serializer_class = SOSerializer


class PCViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    permission_classes = (IsAdmin,)
    queryset = PC.objects.all()
    serializer_class = PCSerializer


class PC_SO_SWViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    permission_classes = (IsAdmin,)
    queryset = PC_SO_SW.objects.all()
    serializer_class = PC_SO_SWSerializer

class SW_ClassroomViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdmin,)
    queryset = SW_Classroom.objects.all()
    serializer_class = SW_ClassroomSerializer


class SWViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdmin,)
    queryset = SW.objects.all()
    serializer_class = SWSerializer


class AllClassrooms(APIView):
    """
    Retorna todas las aulas informáticas
    """
    
    def get(self,request):
        aulas = Classroom.objects.all()
        salida = []
        for aula in aulas:
            salida.append({"id":aula.pk,"title":str(aula)})
        return Response(salida)


class ClassroomAvailability(APIView):
    """
    Retorna si un aula se encuentra disponible para satisfacer una Solicitud de reserva específica 
    """
    permission_classes = (IsAdmin,)

    def get(self, request, pk_solicitud, pk_aula):
        #solicitud
        rq = Request.objects.get(pk=pk_solicitud)
        #dias asociados a una solicitud
        dias_solicitud = []
        for slot in Slots.objects.filter(request__exact=rq):
            dias_solicitud.append(slot.day)
        #reservas vinculadas a un aula
        rvaua = Reservation.objects.filter(classroom__exact=pk_aula)
        #slots reservados para un determinado aula y que coincide con los dias de la solicitud (RSi)
        slots_reservados = Slots.objects.filter(reservation__in = rvaua).filter(day__in=dias_solicitud)
        #slots de la solicitud (RQi)
        slots_solictiados = Slots.objects.filter(request__exact=rq)
        # rq = request, rs = reservation
        # SI (rq.et > rs.st AND rq.st >= rs.st AND rq.st < rs.et) ENTONCES False
        for rq in slots_solictiados:
            for rs in slots_reservados:
                #caso1
                if (rq.start_time < rs.start_time and rq.end_time > rs.start_time):
                    return Response(False)
                #caso2
                if (rq.start_time >= rs.start_time and rq.end_time <= rs.end_time):
                    return Response(False)
                #caso3
                if (rq.start_time >= rs.start_time and rq.start_time < rs.end_time):
                    return Response(False)
        return Response(True)

class SWRequirementsCheck(APIView):
    """
    Chequea si un aula cumple los requisitos de sw especificados en una solicitud de reserva
    """
    permission_classes = (IsAdmin,)

    def get(self, request, pk_solicitud, pk_aula):
        #solicitud "rq"
        rq = Request.objects.get(pk=pk_solicitud)
        #dias asociados a una solicitud
        sws_rq = []
        for sw in rq.swsRequired.all():
            sws_rq.append(sw)
        sws_aula = []
        sw_pc = {} #cantidad de pcs que tienen un determinado sw
        aula = Classroom.objects.get(pk=pk_aula)
        for pc in PC.objects.filter(classroom__exact=aula): #iteramos las pcs del aula
            for pc_so in PC_SO.objects.filter(pc__exact=pc): #iteramos los sistemas operativos de las pcs
                for pc_so_sw in PC_SO_SW.objects.filter(pc_so__exact=pc_so): #iteramos los sw dentro de una dupla PC_SO
                    sws_aula.append(pc_so_sw.sw) #añadirmos el sw a los sws del aula
                    #contamos la cantidad de pcs que tienen un determinado sw
                    if pc_so_sw.sw in sws_rq:
                        if str(pc_so_sw.sw) in sw_pc.keys():
                            sw_pc[str(pc_so_sw.sw)] += 1
                        else:
                            sw_pc[str(pc_so_sw.sw)] = 1
        #finalmente necesitamos comparar ambos conjuntos, para esto usamos conjuntos:
        sws_rq = set(sws_rq)
        sws_aula = set(sws_aula)

        #para que se cumplan los requisitos el conjunto sws_aula debe contener a sws_solicitud

        response = Response(json.dumps({
            "exists": (sws_rq.issubset(sws_aula)),
            "number_of_pcs": (sw_pc)
        }))
        return response


class Slots_Reservation_Day(APIView):

    permission_classes = (IsAdminOrBecario,)

    def get(self,request,day,month,year):
        fecha = datetime.datetime(int(year),int(month),int(day)) #las fechas se cargan asi
        str_fecha = fecha.strftime("%d/%m/%Y")
        salida = []
        slots = Slots.objects.filter(day__exact=fecha)
        i = 1
        for slot in slots:
            if not slot.reservation is None:
                reservation_id = slot.reservation.pk
                reservation = Reservation.objects.get(pk=reservation_id)
                classroom = Classroom.objects.get(pk__exact=reservation.classroom.pk)
                start_time = datetime.datetime(int(year),int(month),int(day),slot.start_time.hour,slot.start_time.minute)
                end_time = datetime.datetime(int(year),int(month),int(day),slot.end_time.hour,slot.end_time.minute)
                salida.append(
                        {
                            "id": str(i),
                            "resourceId": str(classroom.pk),
                            "start": str(start_time),
                            "end": str(end_time),
                            "title": reservation.name
                        }
                    )
                i += 1
        return Response(salida)

class Slots_Reservation(APIView):

    permission_classes = (IsAdminOrBecario,)

    def get(self, request):
        start = request.GET.get('start')
        end = request.GET.get('end')

        fecha = start[0:10]
        end_date = end[0:10]
        year = start[0:4]
        month = start[5:7]
        day = start[8:10]

        salida = []
        slots = Slots.objects.filter(day__exact=fecha)
        i = 1
        for slot in slots:
            if not slot.reservation is None:
                reservation_id = slot.reservation.pk
                reservation = Reservation.objects.get(pk=reservation_id)
                classroom = Classroom.objects.get(pk__exact=reservation.classroom.pk)
                start_time = datetime.datetime(int(year), int(month), int(day), slot.start_time.hour,
                                               slot.start_time.minute)
                end_time = datetime.datetime(int(year), int(month), int(day), slot.end_time.hour, slot.end_time.minute)
                salida.append(
                    {
                        "id": str(i),
                        "resourceId": str(classroom.pk),
                        "start": str(start_time),
                        "end": str(end_time),
                        "title": reservation.name
                    }
                )
                i += 1
        return Response(salida)

class Slots_Reservation_Range(APIView):
    """
    Retorna todos los Slots de reservas en un rango de fechas
    """
    permission_classes = (IsAdminOrBecario,)

    def get(self,request,start_day,start_month,start_year,end_day,end_month,end_year):
        start_date = datetime.datetime(int(start_year),int(start_month),int(start_day)) #las fechas se cargan asi
        end_date = datetime.datetime(int(end_year),int(end_month),int(end_day)) #las fechas se cargan asi
        str_start_date = start_date.strftime("%d/%m/%Y")
        str_end_date = end_date.strftime("%d/%m/%Y")
        salida = []
        #loopear por un rango de fechas: https://stackoverflow.com/questions/7274267/print-all-day-dates-between-two-dates
        delta = end_date - start_date # timedelta
        for i in range(delta.days + 1):
            slots = Slots.objects.filter(day__exact=(start_date + timedelta(i)))
            for slot in slots:
                if not slot.reservation is None:
                    reservation_id = slot.reservation.pk
                    reservation = Reservation.objects.get(pk=reservation_id)
                    classroom = Classroom.objects.get(pk__exact=reservation.classroom.pk)
                    reservation_start_time = datetime.datetime(int(slot.day.year),int(slot.day.month),int(slot.day.day),slot.start_time.hour,slot.start_time.minute)
                    reservation_end_time = datetime.datetime(int(slot.day.year),int(slot.day.month),int(slot.day.day),slot.end_time.hour,slot.end_time.minute)
                    # reservation_start_time = "{}-{}-{}T{}:{}:{}".format((slot.day.year),(slot.day.month),(slot.day.day),slot.start_time.hour,slot.start_time.minute,slot.start_time.second)
                    # reservation_end_time = "{}-{}-{}T{}:{}:{}".format((slot.day.year),(slot.day.month),(slot.day.day),slot.end_time.hour,slot.end_time.minute,slot.end_time.second)
                    colors = ['#eeeeee','#dddddd','#cccccc','#bbbbbb','#aaaaaa']

                    salida.append(
                            {
                                "title": reservation.name,
                                "start": str(reservation_start_time),
                                "end": str(reservation_end_time),
                                "resourceId": str(classroom.pk),
                                "request_owner" : str(reservation.request.owner),#"aula: {}, inicio: {}. fin: {}".format(classroom.number,reservation_start_time,reservation_end_time),
                                "color" : secrets.choice(colors)
                                # "request_owner" : reservation.request.owner
                            }
                        )

        return Response(salida)



class Slots_Reservation_Range_Specific_User(APIView):
    """
    Retorna todos los Slots de reservas en un rango de fechas de un usuario específico
    """

    def get(self,request,start_day,start_month,start_year,end_day,end_month,end_year,user_id):
        start_date = datetime.datetime(int(start_year),int(start_month),int(start_day)) #las fechas se cargan asi
        end_date = datetime.datetime(int(end_year),int(end_month),int(end_day)) #las fechas se cargan asi
        str_start_date = start_date.strftime("%d/%m/%Y")
        str_end_date = end_date.strftime("%d/%m/%Y")
        salida = []
        #loopear por un rango de fechas: https://stackoverflow.com/questions/7274267/print-all-day-dates-between-two-dates
        delta = end_date - start_date # timedelta
        for i in range(delta.days + 1):
            slots = Slots.objects.filter(day__exact=(start_date + timedelta(i)))
            for slot in slots:
                if not (slot.reservation is None):
                    if (int(slot.reservation.request.owner.pk) == int(user_id)):
                        reservation_id = slot.reservation.pk
                        reservation = Reservation.objects.get(pk=reservation_id)
                        classroom = Classroom.objects.get(pk__exact=reservation.classroom.pk)
                        reservation_start_time = datetime.datetime(int(slot.day.year),int(slot.day.month),int(slot.day.day),slot.start_time.hour,slot.start_time.minute)
                        reservation_end_time = datetime.datetime(int(slot.day.year),int(slot.day.month),int(slot.day.day),slot.end_time.hour,slot.end_time.minute)
                        # reservation_start_time = "{}-{}-{}T{}:{}:{}".format((slot.day.year),(slot.day.month),(slot.day.day),slot.start_time.hour,slot.start_time.minute,slot.start_time.second)
                        # reservation_end_time = "{}-{}-{}T{}:{}:{}".format((slot.day.year),(slot.day.month),(slot.day.day),slot.end_time.hour,slot.end_time.minute,slot.end_time.second)
                        colors = ['#eeeeee','#dddddd','#cccccc','#bbbbbb','#aaaaaa']

                        salida.append(
                                {
                                    "title": reservation.name,
                                    "start": str(reservation_start_time),
                                    "end": str(reservation_end_time),
                                    "resourceId": str(classroom.pk),
                                    "request_owner" : str(reservation.request.owner),#"aula: {}, inicio: {}. fin: {}".format(classroom.number,reservation_start_time,reservation_end_time),
                                    "color" : secrets.choice(colors)
                                    # "request_owner" : reservation.request.owner
                                }
                            )
        return Response(salida)


class Reservations_slot_subgroup_update(APIView):
    """
    Acualiza un subgrupo de Slots de una Reserva
    """
    permission_classes = (IsAdmin,)

    def post(self,request,pk_reserva):
        slots = list(Slots.objects.filter(reservation_id=pk_reserva))
        
        #REFACTOREAR  esta funcion esta en la vista reservation_edit, considerar encapsularla en una funcion

        #obtener subgrupos ,es decir, diccionario "sg"
        i = 0
        sg = {} #subgrupos: conjunto de subrupos de slots que comparten mismo dia(ej: Lunes) hora inicio y hora fin
        ss = slots #sin subgrupos: slots que aun no fueron asignadas a un subgrupo
        while (len(ss)>0):
            sg[i]=[ss[0]] #al i-esimo subgrupo asigno el primer slot de las que aun no se clasificaron
            ss.remove((sg[i][0]))
            if(len(ss)>0):
                for j in range(0,len(ss)):
                    #preguntamos si coinciden en la terna (dia(ej:lunes),hora_inicio,hora_fin)
                    if ((sg[i][0].day.strftime("%A"),sg[i][0].start_time,sg[i][0].end_time) == (ss[j].day.strftime("%A"),ss[j].start_time,ss[j].end_time)):
                        sg[i].append(ss[j])
                for slot in sg[i]:
                    try:
                        ss.remove(slot) #estas slots ya pertencen a un subgrupo, por ende no pertencen a "sin subgrupos"
                    except Exception as e:
                        pass
                i += 1
        #FIN REFACTOREAR

        updates = request.data
        
        for key in sg.keys():
            for slot_reference in sg[key]:
                slot = Slots.objects.get(pk__exact=slot_reference.pk)
                inicio = str(updates[key].get('inicio')).split(':')
                fin = str(updates[key].get('fin')).split(':')
                slot.start_time = time(hour=int(inicio[0]),minute=int(inicio[1]))
                slot.end_time = time(hour=int(fin[0]),minute=int(fin[1]))
                slot.save()
        return Response({"Success":True})

class Requests_slot_subgroup_update(APIView):
    """
    Acualiza un subgrupo de Slots de una Solicitu de Reserva
    """

    permission_classes = (IsAdmin,)

    def post(self,request,pk_solicitud):

        slots = list(Slots.objects.filter(request_id=pk_solicitud))

        #REFACTOREAR  esta funcion esta en la vista reservation_edit, considerar encapsularla en una funcion

        #obtener subgrupos ,es decir, diccionario "sg"
        i = 0
        sg = {} #subgrupos: conjunto de subrupos de slots que comparten mismo dia(ej: Lunes) hora inicio y hora fin
        ss = slots #sin subgrupos: slots que aun no fueron asignadas a un subgrupo
        while (len(ss)>0):
            sg[i]=[ss[0]] #al i-esimo subgrupo asigno el primer slot de las que aun no se clasificaron
            ss.remove((sg[i][0]))
            if(len(ss)>0):
                for j in range(0,len(ss)):
                    #preguntamos si coinciden en la terna (dia(ej:lunes),hora_inicio,hora_fin)
                    if ((sg[i][0].day.strftime("%A"),sg[i][0].start_time,sg[i][0].end_time) == (ss[j].day.strftime("%A"),ss[j].start_time,ss[j].end_time)):
                        sg[i].append(ss[j])
                for slot in sg[i]:
                    try:
                        ss.remove(slot) #estas slots ya pertencen a un subgrupo, por ende no pertencen a "sin subgrupos"
                    except Exception as e:
                        pass
                i += 1
        #FIN REFACTOREAR

        updates = request.data

        for key in sg.keys():
            for slot_reference in sg[key]:
                slot = Slots.objects.get(pk__exact=slot_reference.pk)
                inicio = str(updates[key].get('inicio')).split(':')
                fin = str(updates[key].get('fin')).split(':')
                slot.start_time = time(hour=int(inicio[0]),minute=int(inicio[1]))
                slot.end_time = time(hour=int(fin[0]),minute=int(fin[1]))
                slot.save()
        return Response({"Success":True})

class APISlotDelete(APIView):

    permission_classes = (IsAdmin,)

    def get(self,request,pk_slot):
        instance = get_object_or_404(Slots, pk=pk_slot)
        instance.delete()
        return Response({"Success":True})
   
        

# @api_view(['GET'])
# def api_root(request, format=None):
#     return Response({
#         'users': reverse('user-list', request=request, format=format),
#         'classrooms': reverse('classroom-list', request=request, format=format),
#         'sos': reverse('so-list', request=request, format=format),
#         'requests': reverse('request-list', request=request, format=format),
#         'pcs': reverse('pc-list', request=request, format=format),
#         'pc_sos': reverse('pc_so-list', request=request, format=format),
#     })

from django.shortcuts import render

#index
@login_required
def index(request):
    if (request.user.is_teacher):
        asociated_request = Request.objects.filter(owner__exact=request.user)
        reservations = Reservation.objects.filter(request__in=asociated_request)
    else:
        reservations = Reservation.objects.all()
    return render(
            request,
            "index.html",
            {"reservationIndex": reservations,
             "requests": Request.objects.all(),
             "today": HOY
            }
        )

# Aula function-based views
@admin_required
@login_required
def classrooms(request):
    return render(request, "Aula/lista.html", {"aulas": Classroom.objects.all()})


@admin_required
@login_required
def classroom_create(request):
    existen_establecimientos = Establishment.objects.all().exists()
    form = ClassroomForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('aulas')
    return render(
            request,
            "Aula/crear.html",
            {
                'form': form,
                'existen_establecimientos':existen_establecimientos
            }
        )


@admin_required
@login_required
def classroom_edit(request, pk=None):
    instance = get_object_or_404(Classroom, pk=pk)
    form = ClassroomForm(request.POST or None, instance=instance)
    existen_establecimientos = Establishment.objects.all().exists()

    if form.is_valid():
        form.save()
        return HttpResponseRedirect(instance.get_absolute_url())

    return render(
        request, 
        "Aula/crear.html", 
        {
            'aula': instance, 
            'form': form,
            'existen_establecimientos': existen_establecimientos
        }
    )


@login_required
def classroom_detail(request, pk):
    instance = get_object_or_404(Classroom, pk=pk)
    return render(request, "Aula/ver.html", {'aula': instance})


@login_required

def classroom_delete(request, pk=None):
    instance = get_object_or_404(Classroom, pk=pk)
    instance.delete()
    return redirect("aulas")


# SO function-based views
def sos(request):
    return render(
        request,
        "SO/lista.html",
        {"sos": SO.objects.all()}
    )

@admin_required
@login_required
def so_create(request):
    form = SoForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('sos')
    return render(request, 'SO/crear.html', {'form': form})


@admin_required
@login_required
def so_edit(request, pk=None):
    instance = get_object_or_404(SO, pk=pk)
    form = SoForm(request.POST or None, instance=instance)

    if form.is_valid():
        instance = form.save()
        return HttpResponseRedirect(instance.get_absolute_url())

    return render(request, "SO/crear.html", {'so': instance, 'form': form})


@login_required
def so_detail(request, pk):
    instance = get_object_or_404(SO, pk=pk)
    return render(
        request,
        "SO/ver.html",
        {'so': instance}
    )


@admin_required
@login_required
def so_delete(request, pk=None):
    instance = get_object_or_404(SO, pk=pk)
    instance.delete()
    return redirect("sos")


@login_required
def so_by_pc(request):
    pc_id = request.GET.get('pc_id')
    #sos = PC.get(pk=pc_id).so.all()
    sos = get_object_or_404(PC, pk=pc_id).so.all()
    #sos = SO.objects.none()
    # for so_id in sos_id:
    #     so = SO.objects.filter(pk=so_id.so_id)
    #     sos.create(so)
    return render(request, 'SO/soByPC.html', {'sos': sos})


# PC function-based views
@login_required
def pc_clone(request, pk):
    pc = PC.objects.get(pk__exact=pk)
    pc_sos = PC_SO.objects.filter(pc__exact=pc)
    pc_so_sws = PC_SO_SW.objects.filter(pc_so__in=pc_sos)
    aulas = Classroom.objects.all()


    return render(
        request,
        "PC/clonar.html",
        {
            "pc": pc,
            "aulas": aulas,
            "pc_so_sws": pc_so_sws
        }
    )


class ClonePCtoClassroom(APIView):

    permission_classes = (IsAdmin,)
    
    def get_highest_pc_number_in_classroom(self,pk_aula):
        """
        Si el aula tiene pcs retorna el numero mas alto que identifique
        a una ellas.
        
        En el caso de que no exista pcs en el aula retorna cero.
        """
        aula = Classroom.objects.get(pk__exact=pk_aula)
        pcs_aula = PC.objects.filter(classroom__exact=aula)
        if(pcs_aula):
            pc_numbers = [pc.number for pc in pcs_aula]
            return (max(pc_numbers))
        else:
            return 0

    def get (self,request,pk_pc,cantidad,pk_aula):
        #"cantidad" de clones de "pk_pc" a depositar en "pk_aula"
        mensaje = ''
        try:
            pc = PC.objects.get(pk__exact=pk_pc)
            aula_destino = Classroom.objects.get(pk__exact=pk_aula)
            highest_pc_number_in_classroom = self.get_highest_pc_number_in_classroom(pk_aula)
            cantidad = int(cantidad)
            for i in range(cantidad):
                try:
                    new_cloned_pc = PC.objects.create(
                        number= highest_pc_number_in_classroom + i + 1,
                        details= pc.details,
                        classroom=aula_destino
                    )
                    pc_sos = PC_SO.objects.filter(pc__exact=pc)
                    for pc_so in pc_sos:
                        new_cloned_pc_so = PC_SO.objects.create(
                            pc=new_cloned_pc,
                            so=pc_so.so
                        )
                        pc_so_sws = PC_SO_SW.objects.filter(pc_so__exact=pc_so)
                        for pc_so_sw in pc_so_sws:
                            new_cloned_pc_so_sw = PC_SO_SW.objects.create(
                                pc_so = new_cloned_pc_so,
                                sw = pc_so_sw.sw
                            )
                    msj = f'{new_cloned_pc.number} éxito \n'
                    mensaje += msj
                except:
                    msj = f'{new_cloned_pc.number} falló \n'
                    mensaje += msj
        except:
            mensaje = "clonado falló"
        return Response({"mensaje":mensaje})


@admin_required
@login_required
def pcs(request):
    return render(request, "PC/lista.html", {"pcs": PC.objects.all()})


@admin_required
@login_required
def pc_create(request):
    existen_aulas = Classroom.objects.all().exists()
    existen_sos = SO.objects.all().exists()
    form = PcForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()

        for so_id in request.POST.getlist('so'):
            so_ob = SO.objects.get(id=so_id)
            PC_SO.objects.create(pc=instance, so=so_ob)

        return redirect("pcs")
    form.fields['so'].widget.attrs = {'class': 'form-control form-control-chosen'}
    return render(
        request,
        "PC/crear.html",
        {
            'form': form,
            'existen_aulas': existen_aulas,
            'existen_sos': existen_sos
        }
    )


@admin_required
@login_required
def pc_edit(request, pk=None):
    instance = get_object_or_404(PC, pk=pk)
    form = PcForm(request.POST or None, instance=instance)
    existen_aulas = Classroom.objects.all().exists()
    existen_sos = SO.objects.all().exists()

    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()

        PC_SO.objects.filter(pc=instance).delete()

        for so_id in request.POST.getlist('so'):
            so_ob = SO.objects.get(id=so_id)
            PC_SO.objects.create(pc=instance, so=so_ob)

        return HttpResponseRedirect(instance.get_absolute_url())
    form.fields['so'].widget.attrs = {'class': 'form-control form-control-chosen'}
    return render(
        request, "PC/crear.html", 
        {
            'pc': instance, 
            'form': form,
            'existen_aulas': existen_aulas,
            'existen_sos': existen_sos
        }
    )


@login_required
def pc_detail(request, pk):
    pc = get_object_or_404(PC, pk=pk)
    pc_sos = PC_SO.objects.filter(pc__exact=pc)
    pc_so_sws = PC_SO_SW.objects.filter(pc_so__in=pc_sos)
    return render(
        request,
        "PC/ver.html",
        {
            'pc': pc,
            'pc_so_sws':pc_so_sws
        }
    )


@admin_required
@login_required
def pc_delete(request, pk=None):
    instance = get_object_or_404(PC, pk=pk)
    instance.delete()
    return redirect("pcs")


# SW function-based views
@login_required
def sws(request):
    return render(
        request,
        "SW/lista.html",
        {"sws": SW.objects.all()}
    )


@login_required
def sw_create(request):
    form = SwForm(request.POST or None)
    if form.is_valid():
        instance = form.save()
        return redirect("sws")
    return render(request, 'SW/crear.html', {'form': form})


@login_required
def sw_edit(request, pk=None):
    instance = get_object_or_404(SW, pk=pk)
    form = SwForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save()
        return HttpResponseRedirect(instance.get_absolute_url())
    return render(request, "SW/crear.html", {'sw': instance, 'form': form})


@login_required
def sw_detail(request, pk):
    instance = get_object_or_404(SW, pk=pk)
    return render(request, "SW/ver.html", {'sw': instance})


@login_required
def sw_delete(request, pk=None):
    instance = get_object_or_404(SW, pk=pk)
    instance.delete()
    return redirect("sws")


@login_required
def sw_assign(request, template_name='SW/asignarPC.html'):
    existen_sos = SO.objects.all().exists()
    existen_pcs = PC.objects.all().exists()
    existen_sws = SW.objects.all().exists()
    form = SwToPcForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        pc_id = request.POST.get('pc')
        so_id = request.POST.get('so')
        instance.pc_so_id = PC_SO.objects.get(pc_id=pc_id, so_id=so_id).id
        instance.save()
        return redirect("swAssignments")
    form.fields['sw'].widget.attrs = {'class': 'form-control form-control-chosen'}
    return render(
        request,
        template_name,
        {
            'form': form,
            'existen_sos': existen_sos,
            'existen_pcs': existen_pcs,
            'existen_sws': existen_sws
        }
    )


@login_required
def sw_assignments(request, pcs_added=None):
    #scps = sw classroom pc so
    scps = []
    #ejemplo de scps = [{"sw":'sketch up',"classroom":'1.1',"pc":'1.1.1',"so":'ubuntu'}]
    sws = SW.objects.all().order_by("name")
    for sw in sws:
        #defino primero las variables para que sea mas legible

        pc_so_sws_sw = [] #ternas pc so sw que tienen ese sw

        #poblamos las variables
        for pc_so_sw_sw in PC_SO_SW.objects.filter(sw__exact=sw):
            pc_so_sws_sw.append(pc_so_sw_sw)
        for pc_so_sw_sw in pc_so_sws_sw:
                classroom_sw = Classroom.objects.get(id__exact=pc_so_sw_sw.pc_so.pc.classroom.id)
                scps.append(
                    {
                        "id": pc_so_sw_sw.id,
                        "sw": sw,
                        "classroom": classroom_sw,
                        "pc": pc_so_sw_sw.pc_so.pc,
                        "so": pc_so_sw_sw.pc_so.so
                    }
                )
    #csps = classrom sw pc so
    csps = []
    apss = [] #aula pc so sw
    #ejemplo de scps = [{"classroom":'1.1',"sw":'sketch up',"pc":'1.1.1',"so":'ubuntu'}]
    classrooms = Classroom.objects.all().order_by("number")
    for classroom in classrooms:
        pc_so_sws_classroom = []
        pcs_classroom = PC.objects.filter(classroom__exact=classroom)
        pc_sos_classroom = []
        for pc in pcs_classroom:
            for pc_so_classroom in PC_SO.objects.filter(pc__exact=pc):
                    pc_sos_classroom.append(pc_so_classroom)
        for pc_so_classroom in pc_sos_classroom:
            for pc_so_sw_classroom in PC_SO_SW.objects.filter(pc_so__exact=pc_so_classroom):
                pc_so_sws_classroom.append(pc_so_sw_classroom)
        for pc_so_sw_classroom in pc_so_sws_classroom:
            csps.append(
                        {
                        "classroom":classroom,
                        "sw":pc_so_sw_classroom.sw,
                        "pc":pc_so_sw_classroom.pc_so.pc,
                        "so":pc_so_sw_classroom.pc_so.so,
                        "id":pc_so_sw_classroom.id
                        }
                    )
            apss.append(
                        {
                        "classroom":classroom,
                        "pc":pc_so_sw_classroom.pc_so.pc,
                        "so":pc_so_sw_classroom.pc_so.so,
                        "sw":pc_so_sw_classroom.sw,
                        "id":pc_so_sw_classroom.id

                        }
                    )
    return render(
        request,
        "SW/asignaciones.html",
        {
            "scps": scps,
            "csps": csps,
            "apss": apss,
            "pcs_added": pcs_added
        }
    )

@login_required
def sw_assignment_delete(request, pk=None):
    instance = get_object_or_404(PC_SO_SW, pk=pk)
    instance.delete()
    return redirect("swAssignments")


@login_required
def sw_so_classroom_assignment(request):
    existen_sos = SO.objects.all().exists()
    existen_aulas = Classroom.objects.all().exists()
    existen_sws = SW.objects.all().exists()

    if request.method == 'GET':
        pcs_added = None
        template_name = 'SW/asignarAula.html'
    elif request.method == 'POST':
        sw = get_object_or_404(SW, pk=request.POST.get('sw'))
        so = get_object_or_404(SO, pk=request.POST.get('so'))
        aula = get_object_or_404(Classroom, pk=request.POST.get('aula'))
        # Obtenemos todas las PCs con el SO y Aula especificada
        pcs = PC.objects.filter(classroom__exact=aula)
        pc_sos = PC_SO.objects.filter(so__exact=so).filter(pc__in=pcs)
        pcs_added = []
        for pc_so in pc_sos:
            form = PcSoSwForm({"pc_so": pc_so.pk, "sw": sw.pk})
            if form.is_valid():
                form.save()
                pc_so.pc.sw = sw.name
                pcs_added.append(pc_so.pc)
        return sw_assignments(request, pcs_added)

    return render(
        request,
        template_name,
        {
            'aulas': Classroom.objects.all(),
            'sos': SO.objects.all(),
            'sws': SW.objects.all(),
            'existen_sos': existen_sos,
            'existen_aulas': existen_aulas,
            'existen_sws': existen_sws,
        }
    )


# Request function-based views
@login_required
def requests(request):
    return render(
        request,
        "Request/lista.html",
        {"requests": Request.objects.all(),
         "sos": SO.objects.all()}
    )

@login_required
@admin_or_teacher_required
def request_create(request, template_name='Request/crear.html'):
    if request.method == 'GET':
        form = RequestFormOne(request.GET or None)
        slots = SlotsFormset(queryset=Slots.objects.none(), prefix='slots')

    elif request.method == 'POST':
        form = RequestFormOne(request.POST)
        slots = SlotsFormset(request.POST, prefix='slots')
        comments = request.POST.get('comments')
        if form.is_valid() and slots.is_valid():
            instance = form.save(commit=False)
            if request.POST.get('type') == 'punctual':
                instance.repetition = None
                instance.until = None
            if instance.comments == '':
                instance.comments = None
            instance.owner = request.user
            instance.save()

            for slot in slots:
                oneday = slot.save(commit=False)
                oneday.request = instance
                oneday.save()
                if request.POST.get('type') == 'periodic':
                    weeksToAdd = 1 if instance.repetition == 'weekly' else 4
                    nextSlot = copy.deepcopy(oneday)
                    nextSlot.id = None
                    nextSlot.day += timedelta(weeks=weeksToAdd)
                    while nextSlot.day <= instance.until:
                        nextSlot.save()
                        nextSlot = copy.deepcopy(nextSlot)
                        nextSlot.id = None
                        nextSlot.day += timedelta(weeks=weeksToAdd)

            for sw_id in request.POST.getlist('swsRequired'):
                sw_ob = SW.objects.get(id=sw_id)
                Request_SW.objects.create(request=instance, sw=sw_ob)

            # Contenido del mail
            solicitante = instance.owner
            urlsitio = request.build_absolute_uri('/solicitudes/') # el path a donde debe ir es a solicitudes
            if instance.type == 'periodic':
                if instance.repetition == 'weekly':
                    repeticion = 'Semanalmente'
                else:
                    repeticion = 'Mensualmente'
            else:
                repeticion = 'Nunca'
            slotsRequests = Request.objects.get(pk=instance.pk).slots_set.all()
            admin = 'administrador'
            subject = 'Notificación de solicitud - SysLab'
            html_message = render_to_string('Email/request.html',
                {   # Contenido dinamico de la reserva, aca van los datos de fecha, horario, etc
                    'solicitante': solicitante,
                    'admin': admin,
                    'repeticion': repeticion,
                    'slotsRequests': slotsRequests,
                    'urlSitio': urlsitio,
                    'comments': comments
                })
            plain_message = strip_tags(html_message) # sacar los tags '< >' para ver contenido aunque sea
            email_from = settings.EMAIL_HOST_USER # emisor del email // host
            recipient_list = [settings.EMAIL_RECIPIENT] # emails receptor formato Strings
            msg = EmailMultiAlternatives(subject, plain_message, email_from, recipient_list)
            msg.attach_alternative(html_message, "text/html")
            msg.send()

            return redirect("requests")
    form.fields['swsRequired'].widget.attrs = {'class': 'form-control form-control-chosen'}
    return render(request, template_name, {'form': form, 'slots': slots})


@login_required
def request_detail(request, pk):
    if request.method == 'GET':
        instance = get_object_or_404(Request, pk=pk)
        sw_requirements = Request_SW.objects.filter(request__exact=instance)
        reservation = []
        if instance.status == "approved":
            reservation = Reservation.objects.get(request__exact=instance)
        return render(
            request,
            "Request/ver.html",
            {
                'request': instance,
                'reservation': reservation,
                'classrooms': Classroom.objects.all(),
                'form': RequestToReservationForm,
                'sw_requirements': sw_requirements,
             }
        )
    elif request.method == 'POST':
        form = RequestToReservationForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.request_id = pk
            instance.save()
            slots = Request.objects.get(pk=pk).slots_set.all()
            # modificar status de solicitud a approved
            asociated_request = Request.objects.get(pk=pk)
            asociated_request.status = "approved"
            asociated_request.save()
            # Notificación de reserva mediante email
            solicitante = asociated_request.owner
            aula = instance.classroom
            nombreReserva = instance.name
            urlsitio = request.build_absolute_uri('/reservas/listado/') # url para el solicitante
            subject = 'Notificación de Reservas - SysLab'
            html_message = render_to_string('Email/reservation.html',
                {   # Contenido dinamico de la reserva, aca van los datos de fecha, horario, etc
                    'solicitante': solicitante,
                    'nombreReserva': nombreReserva,
                    'aula': aula,
                    'slotsRequests': slots,
                    'urlSitio': urlsitio,
                })
            plain_message = strip_tags(html_message) # sacar los tags '< >' para ver contenido aunque sea
            email_from = settings.EMAIL_HOST_USER # emisor del email // host
            recipient_list = [settings.EMAIL_RECIPIENT] # emails receptor formato Strings
            msg = EmailMultiAlternatives(subject, plain_message, email_from, recipient_list)
            msg.attach_alternative(html_message, "text/html")
            msg.send()
            for slot in slots:
                slot.reservation = instance
                slot.save()

            return redirect("reservations")

"""
def sw_edit(request, pk=None):
    instance = get_object_or_404(SW, pk=pk)
    form = SwForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return HttpResponseRedirect(instance.get_absolute_url())
    return render(
        request,
        "SW/editar.html",
        {'sw': instance, 'form': form}
    )
"""


@login_required
def request_edit(request, pk=None):
    solicitud = get_object_or_404(Request, pk=pk)
    slots = list(Slots.objects.filter(request__exact=solicitud))
    #obtener subgrupos ,es decir, diccionario "sg"
    i = 0
    sg = {} #subgrupos: conjunto de subrupos de slots que comparten mismo dia(ej: Lunes) hora inicio y hora fin
    ss = slots #sin subgrupos: slots que aun no fueron asignadas a un subgrupo
    while (len(ss)>0):
        sg[i]=[ss[0]] #al i-esimo subgrupo asigno el primer slot de las que aun no se clasificaron
        ss.remove((sg[i][0]))
        if(len(ss)>0):
            for j in range(0,len(ss)):
                #preguntamos si coinciden en la terna (dia(ej:lunes),hora_inicio,hora_fin)
                if ((sg[i][0].day.strftime("%A"),sg[i][0].start_time,sg[i][0].end_time) == (ss[j].day.strftime("%A"),ss[j].start_time,ss[j].end_time)):
                    sg[i].append(ss[j])
            for slot in sg[i]:
                try:
                    ss.remove(slot) #estas slots ya pertencen a un subgrupo, por ende no pertencen a "sin subgrupos"
                except Exception as e:
                    pass
            i += 1
    #creamos una lista para la view
    subgrupos = []
    for key in sg.keys():
        subgrupos.append(
                {
                    "id": key,
                    "dia": sg[key][0].day.strftime("%A"),
                    "inicio": sg[key][0].start_time,
                    "fin": sg[key][0].end_time,
                    "ranuras": sg[key]
                }
            )
    return render(
        request,
        "Request/editar.html",
        {
            "solicitud": solicitud,
            "subgrupos": subgrupos,
        }
    )


@login_required
def request_delete(request, pk=None):
    instance = get_object_or_404(Request, pk=pk)
    # Notificación de solicitudes eliminadas por docente/admin mediante email
    slotsRequests = Request.objects.get(pk=instance.pk).slots_set.all()
    solicitante = instance.owner
    asociated_request = instance.pk
    urlsitio = request.build_absolute_uri('/solicitudes/') # url
    subject = 'Notificación de Reservas - SysLab'
    html_message = render_to_string('Email/requestDelete.html',
        {   # Contenido dinamico de la reserva, aca van los datos de fecha, horario, etc
            'solicitante': solicitante,
            'slotsRequests': slotsRequests,
            'urlSitio': urlsitio,
            'asociatedRequest': asociated_request
        })
    plain_message = strip_tags(html_message) # sacar los tags '< >' para ver contenido aunque sea
    email_from = settings.EMAIL_HOST_USER # emisor del email // host
    if (request.user.is_teacher):
        recipient_list = [settings.EMAIL_HOST_USER] # email caso si el docente es quien elimina
    else:
        recipient_list = [settings.EMAIL_RECIPIENT] # en caso que el admin elimine la request se envia al docente
    msg = EmailMultiAlternatives(subject, plain_message, email_from, recipient_list)
    msg.attach_alternative(html_message, "text/html")
    msg.send()
    instance.delete()
    return redirect("requests")


class APIrequest_reject(APIView):

    permission_classes = (IsAdmin,)

    def post(self,request,pk_solicitud):

        instance = get_object_or_404(Request, pk=pk_solicitud)
        solicitante = instance.owner
        urlsitio = request.build_absolute_uri('/solicitudes/')
        subject = 'Notificación de Solicitudes - SysLab'
        asociated_request = instance.pk
        motivo = request.data 
        html_message = render_to_string('Email/requestReject.html', 
            {
                'solicitante': solicitante,
                'urlSitio': urlsitio,
                'asociatedRequest': asociated_request,
                'motivo': motivo 
            })
        plain_message = strip_tags(html_message)
        email_from = settings.EMAIL_HOST_USER
        recipient_list = [settings.EMAIL_RECIPIENT]
        msg = EmailMultiAlternatives(subject, plain_message, email_from, recipient_list)
        msg.attach_alternative(html_message, "text/html")
        msg.send()
        instance.status = "rejected"
        instance.save()
        return Response(True)

# Reservations function-based views
@login_required
def reservations(request):
    return render(
        request,
        "Reservation/calendario.html",
        {"reservations": Reservation.objects.all()}
    )

@login_required
def reservations_listado(request): #REFACTOREAR
    #Se filtran las reservas a mostrar segun el tipo de usuario
    if (request.user.is_teacher):
        asociated_request = Request.objects.filter(owner__exact=request.user)
        reservations = Reservation.objects.filter(request__in=asociated_request)
    else:
        reservations = Reservation.objects.all()
    return render(
            request,
            "Reservation/lista.html",
            {
                "reservations":reservations,
            }
        )

@admin_required
@login_required
def reservation_edit(request, pk):
    reserva = get_object_or_404(Reservation, pk=pk)
    slots = list(Slots.objects.filter(reservation__exact=reserva))
    #obtener subgrupos ,es decir, diccionario "sg"
    i = 0
    sg = {} #subgrupos: conjunto de subrupos de slots que comparten mismo dia(ej: Lunes) hora inicio y hora fin
    ss = slots #sin subgrupos: slots que aun no fueron asignadas a un subgrupo
    while (len(ss)>0):
        sg[i]=[ss[0]] #al i-esimo subgrupo asigno el primer slot de las que aun no se clasificaron
        ss.remove((sg[i][0]))
        if(len(ss)>0):
            for j in range(0,len(ss)):
                #preguntamos si coinciden en la terna (dia(ej:lunes),hora_inicio,hora_fin)
                if ((sg[i][0].day.strftime("%A"),sg[i][0].start_time,sg[i][0].end_time) == (ss[j].day.strftime("%A"),ss[j].start_time,ss[j].end_time)):
                    sg[i].append(ss[j])
            for slot in sg[i]:
                try:
                    ss.remove(slot) #estas slots ya pertencen a un subgrupo, por ende no pertencen a "sin subgrupos"
                except Exception as e:
                    pass
            i += 1
    #creamos una lista para la view
    subgrupos = []
    for key in sg.keys():
        subgrupos.append(
                {
                    "id": key,
                    "dia": sg[key][0].day.strftime("%A"),
                    "inicio": sg[key][0].start_time,
                    "fin": sg[key][0].end_time,
                    "ranuras": sg[key]
                }
            )
    return render(
            request,
            "Reservation/editar.html",
            {
                "reserva":reserva,
                "subgrupos":subgrupos,
            }
        )

@login_required
def reservation_detail(request, pk):
    reserva = get_object_or_404(Reservation, pk=pk)
    slots = list(Slots.objects.filter(reservation__exact=reserva))
    #obtener subgrupos ,es decir, diccionario "sg"
    i = 0
    sg = {} #subgrupos: conjunto de subrupos de slots que comparten mismo dia(ej: Lunes) hora inicio y hora fin
    ss = slots #sin subgrupos: slots que aun no fueron asignadas a un subgrupo
    while (len(ss)>0):
        sg[i]=[ss[0]] #al i-esimo subgrupo asigno el primer slot de las que aun no se clasificaron
        ss.remove((sg[i][0]))
        if(len(ss)>0):
            for j in range(0,len(ss)):
                #preguntamos si coinciden en la terna (dia(ej:lunes),hora_inicio,hora_fin)
                if ((sg[i][0].day.strftime("%A"),sg[i][0].start_time,sg[i][0].end_time) == (ss[j].day.strftime("%A"),ss[j].start_time,ss[j].end_time)):
                    sg[i].append(ss[j])
            for slot in sg[i]:
                try:
                    ss.remove(slot) #estas slots ya pertencen a un subgrupo, por ende no pertencen a "sin subgrupos"
                except Exception as e:
                    pass
            i += 1
    #creamos una lista para la view
    subgrupos = []
    for key in sg.keys():
        subgrupos.append(
                {
                    "id": key,
                    "dia": sg[key][0].day.strftime("%A"),
                    "inicio": sg[key][0].start_time,
                    "fin": sg[key][0].end_time,
                    "ranuras": sg[key]
                }
            )
    return render(
            request,
            "Reservation/ver.html",
            {
                "reservation": reserva,
                "subgrupos": subgrupos,
            }
        )


@admin_required
@login_required
def reservation_delete(request, pk=None):
    reservation = get_object_or_404(Reservation, pk=pk)
    #casacada artificial, si eliminamos una reserva se borra su solicitud
    try:
        request_asociated = Request.objects.get(pk__exact=reservation.request.pk)
        request_asociated.status = 'deletedReservation'
        request_asociated.save()
    except expression as identifier:
        pass
    reservation.delete()
    return redirect("reservations_listado")



@login_required
def slots_reservation_create(request, pk_reserva):
    reservation = get_object_or_404(Reservation, pk=pk_reserva)
    if request.method == "GET":
        form = SlotsForm(
                {
                    "day": HOY,
                    "start_time": "",
                    "end_time": ""
                }
            )
        form.fields['start_time'].widget.attrs = {'class': 'timeinput start'}
        form.fields['end_time'].widget.attrs = {'class': 'timeinput end'}
        return render(request, "Slots/crear.html", {'form': form, 'reservation': reservation})
    if request.method == "POST":
        form = SlotsForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.request = get_object_or_404(Request, pk=reservation.request.pk)
            instance.reservation = reservation
            instance.save()
            return HttpResponseRedirect("/reservas/{}/editar/".format(reservation.pk))


@admin_required
@login_required
def slots_edit(request, pk=None):
    instance = get_object_or_404(Slots, pk=pk)
    form = SlotsForm(request.POST or None, instance=instance)

    if form.is_valid():
        instance = form.save()
        # La logica aca es que si ya existe la reserva no podes editar la solicitud, si aun no exite editas la solicitud
        try:
            return HttpResponseRedirect("/reservas/{}/editar/".format(instance.reservation.pk))
        except:
            return HttpResponseRedirect("/solicitudes/{}/editar/".format(instance.request.pk))
    form.fields['start_time'].widget.attrs = {'class': 'timeinput start'}
    form.fields['end_time'].widget.attrs = {'class': 'timeinput end'}
    return render(request, "Slots/editar.html", {'instance': instance, 'form': form})

@login_required
def slots_detail(request, pk=None):
    instance = get_object_or_404(Slots, pk=pk)
    form = SlotsForm(request.POST or None, instance=instance)

    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return HttpResponseRedirect("/reservas/{}/editar/".format(instance.reservation.pk))
    return render(
            request,
            "Slots/ver.html",
            {'instance': instance, 'form': form}
        )


@login_required
def slots_request_create(request, pk_solicitud):
    solicitud = get_object_or_404(Request, pk=pk_solicitud)
    if request.method == "GET":
        form = SlotsForm(
                {
                    "day":HOY,
                    "start_time":"",
                    "end_time":""
                }
            )
        return render(
            request,
            "Slots/crear.html",
            {'form': form}
        )
    if request.method == "POST":
        form = SlotsForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.request = solicitud
            instance.save()
            return HttpResponseRedirect("/solicitudes/{}/editar/".format(solicitud.pk))


@admin_required
@login_required
def establishments(request):
    return render(
        request,
        "Establecimiento/lista.html",
        {"establecimientos": Establishment.objects.all()}
    )


@admin_required
@login_required
def establishment_create(request):
    form = EstablishmentForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('establecimientos')
    return render(request, "Establecimiento/crear.html", {'form': form})

@admin_required
@login_required
def establishment_edit(request, pk=None):
    instance = get_object_or_404(Establishment, pk=pk)
    form = EstablishmentForm(request.POST or None, instance=instance)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect(instance.get_absolute_url())

    return render(request, "Establecimiento/crear.html", {'establecimiento': instance, 'form': form})


@admin_required
@login_required
def establishment_detail(request, pk):
    instance = get_object_or_404(Establishment, pk=pk)
    return render(
        request,
        "Establecimiento/ver.html",
        {'establecimiento': instance}
    )


@admin_required
@login_required
def establishment_delete(request, pk=None):
    instance = get_object_or_404(Establishment, pk=pk)
    instance.delete()
    return redirect("establecimientos")


# class LabUserSignUpView(CreateView):
#     model = User
#     form_class = LabUserSignUpForm
#     template_name = "Registration/singup.html"

#     def get_context_data(self, **kwargs):
#         kwargs['user_type'] = 'labUser'
#         return super().get_context_data(**kwargs)

#     def form_valid(self, form):
#         user = form.save()
#         login(self.request, user)
#         return redirect('index')
