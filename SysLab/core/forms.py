from django import forms
from .models import (
    PC, 
    PC_SO_SW, 
    Classroom, 
    SW, 
    SO, 
    Request, 
    Slots, 
    Reservation, 
    Establishment,
    Request_SW, 
    SW_Classroom
)

from django.forms import HiddenInput, inlineformset_factory, \
    DateTimeInput, modelformset_factory
# User types
from django.contrib.auth.forms import UserCreationForm
from .models import User


class EstablishmentForm(forms.ModelForm):
    class Meta(object):
        model = Establishment
        fields = ('name', 'address')
        labels = {
            'name': 'Nombre',
            'address': 'Dirección',
        }


class ClassroomForm(forms.ModelForm):
    class Meta(object):
        model = Classroom
        fields = ('number', 'address', 'floor', 'capacity', 'feature')
        labels = {
            'number': 'Número',
            'address': 'Establecimiento',
            'floor': 'Piso',
            'capacity': 'Capacidad',
            'feature': 'Caracteristicas',
        }


class SoForm(forms.ModelForm):
    class Meta(object):
        model = SO
        fields = ('name', 'description', 'virtualized')
        labels = {
            'name': 'Nombre',
            'description': 'Descripción',
            'virtualized': 'Virtualizado',
        }


class PcForm(forms.ModelForm):
    class Meta(object):
        model = PC
        fields = ('number', 'details', 'classroom', 'so')
        labels = {
            'number': 'Número',
            'details': 'Detalles',
            'classroom': 'Aula',
            'so': 'Sistema Operativo',
        }


class SlotsForm(forms.ModelForm):
    class Meta(object):
        model = Slots
        fields = ('day', 'start_time', 'end_time')
        labels = {
            'day': 'Día',
            'start_time': 'Inicio',
            'end_time': 'Fin',
        }


class SwForm(forms.ModelForm):
    class Meta(object):
        model = SW
        fields = ('name', 'description')
        labels = {
            'name': 'Nombre',
            'description': 'Descripción',
        }


class SwToClassroomForm(forms.ModelForm):
    class Meta(object):
        model = SW_Classroom
        fields = ('sw', 'classroom')
        labels = {
            'sw': 'Sw',
            'classroom': 'Aula'
        }


class PcSoSwForm(forms.ModelForm):
    class Meta(object):
        model = PC_SO_SW
        fields = ("pc_so", "sw")


class SwToPcForm(forms.ModelForm):
    class Meta(object):
        model = PC_SO_SW
        fields = ('sw',)

    sw = forms.ModelChoiceField(queryset=SW.objects.all())
    pc = forms.ModelChoiceField(queryset=PC.objects.all())
    so = forms.ModelChoiceField(queryset=SO.objects.none())

    # Valida que el so elegido sea uno de lo que tenga la PC
    # https://simpleisbetterthancomplex.com/tutorial/2018/01/29/how-to-implement-dependent-or-chained-dropdown-list-with-django.html
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['so'].queryset = SO.objects.none()

        if 'so' in self.data:
            try:
                pc_id = int(self.data.get('pc'))
                self.fields['so'].queryset = SO.objects.filter(pc_so__pc_id=pc_id)
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset


class RequestForm(forms.ModelForm):
    class Meta(object):
        model = Request
        exclude = ()


requestFormSet = inlineformset_factory(Request, Slots, form=RequestForm, extra=1)


class RequestFormOne(forms.ModelForm):
    until = forms.DateField(input_formats=["%d/%m/%Y"], required=False)
    repetition = forms.CharField(required=False)

    class Meta(object):
        model = Request
        fields = ('type', 'requirements', 'amountOfStudents', 'repetition', 'until', 'swsRequired', 'comments')
        labels = {
            'type': 'Tipo de Reserva',
            'requirements': 'Requerimientos de infraestructura (Opcional)',
            'amountOfStudents': 'Cantidad de estudiantes',
            'repetition': 'Repeticion',
            'until': 'Hasta',
            'swsRequired': 'Software solicitado',
            'comments': 'Comentarios',
        }


class RequestPeriodic(forms.ModelForm):
    until = forms.DateField(input_formats=["%d/%m/%Y"], required=False)
    repetition = forms.CharField(required=False)

    class Meta(object):
        model = Request
        fields = ('repetition', 'until')


class SlotRequest(forms.ModelForm):
    class Meta(object):
        model = Slots
        fields = ('day', 'start_time', 'end_time')
        labels = {
            'day': 'Día',
            'start_time': 'Hora de Inicio',
            'end_time': 'Hora de Fin',
        }
    
    start = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'])
    end = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'])


SlotsFormset = modelformset_factory(
    Slots,
    fields=('day', 'start_time', 'end_time', ),
    extra=1,
    widgets={'day': DateTimeInput(format="%d/%m/%y")}
)


class RequestToReservationForm(forms.ModelForm):
    class Meta(object):
        model = Reservation
        fields = ('name', 'classroom')
        labels = {
            'name': 'Nombre de reserva',
            'classroom': 'Aulas',
        }

    classroom = forms.ModelChoiceField(queryset=Classroom.objects.all())


class LabUserSignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User

    def save(self, commit=True):
        user = super().save(commit=False)
        user.labUser = True
        if commit:
            user.save()
        return user


RequestSwFormset = modelformset_factory(
    Request_SW,
    fields=('sw',),
    extra=1,
)
