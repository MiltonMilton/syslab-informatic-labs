(function($) {
  "use strict"; // Start of use strict
  var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    $('.fechaInicio').datepicker({
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: today,
        maxDate: function () {
            return $('.fechaFin').val();
        }
    });
    $('.fechaFin').datepicker({
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: function () {
            return $('.fechaInicio').val();
        }
    });

})(jQuery); // End of use strict
