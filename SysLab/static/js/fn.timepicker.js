$(document).ready(function(){
    $('.timeinput.start').timepicker({
        'minTime': '7:00',
        'maxTime': '23:00',
        'step': 15,
        'timeFormat': 'G:i',
        'appendTo': '.card'
    });

    $('.timeinput.end').timepicker({
        'minTime': '7:30',
        'maxTime': '23:30',
        'step': 15,
        'timeFormat': 'G:i',
        'appendTo': '.card'
    });
});